### State variables ###
ACCUMULATE = 0
COMPUTE = 1
ERROR=2
DISPLAY=3
REACCUMULATE = 4

def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False


def is_operator(s):
    return s in ["+", "-", "*", "/"]

def can_eval(expr):
    return expr[0] != '' and expr[1][0] != '' and expr[1][1] != ''

def evaluate(lhs, rhs, opr):
    lhs = float(lhs)
    rhs = float(rhs)
    if opr == '+':
        return lhs + rhs
    elif opr == '-':
        return lhs - rhs
    elif opr == '*':
        return lhs * rhs
    elif opr == '/':
        return lhs / rhs

#mega function state machine that does the parsing
#this should be broken up since a lot of code
#is repeated
def parse(input_str, start): 
    expression_stack = []
    expression = [ '', [ '', '' ]]
    ndx = 0
    state = ACCUMULATE
    lhs = ''
    has_decimal = False

    #get lhs of first expression
    #append digits to the lhs until we find whitespace
    #make sure to only allow one decimal point
    while is_number(input_str[ndx]) or  input_str[ndx] == '.':
        if input_str[ndx] == '.':
            if not has_decimal:
                lhs += input_str[ndx]
                has_decimal = True
            else:
                print("Syntax error parsing decimal.")
                quit()
        elif input_str[ndx] == '(':
            lhs = parse(input_str, ndx+1)
        else:
            lhs += input_str[ndx]

        ndx += 1

    expression[1][0] = lhs
    lhs = ''
    if input_str[ndx] == ' ':
        ndx +=1


    state = COMPUTE
    #step 1 is get the operator
    while ndx < len(input_str) or (ndx < len(input_str) and input_str[ndx] != ')'):
   
        #discard spaces
        if input_str[ndx] == " ":
            ndx+=1
            continue

        #get the lhs of the expression
        if state == ACCUMULATE:
            had_decimal = False

            while is_number(input_str[ndx]) or input_str[ndx] == '.':
                #don't allow more than one decimal point
                if input_str[ndx] == '.':
                    if not has_decimal:
                        lhs += input_str[ndx]
                        has_decimal = True
                    else:
                        print("Syntax error parsing decimal.")
                        quit()
                elif input_str[ndx] == '(':
                    lhs = parse(input_str, ndx+1)
                else:
                    lhs += input_str[ndx]
                ndx += 1

            expression[1][0] = lhs
            lhs = ''
            if input_str[ndx] == ' ':
                ndx += 1

            if is_number(input_str[ndx]) or input_str[ndx] == '.':
                state = ERROR

        elif state == COMPUTE:
            #determine order of precedence
            if len(expression_stack) == 0:
                expression[0] = input_str[ndx]
            else:
                prev_expr=expression_stack[len(expression_stack) - 1]
                if prev_expr[0] in ["+","-"] and input_str[ndx] in ["*","/"]:
                    #lhs of current expression == rhs of previous expression
                    expression[1][0] = prev_expr[1][1]
                    prev_expr[1][1] = ''
                    #set operator of current expression
                    expression[0] = input_str[ndx]
                    expression_stack.pop()
                    expression_stack.append(prev_expr)
                else:
                    expression[0] = input_str[ndx]
            state=REACCUMULATE
            ndx+=1
        elif state == REACCUMULATE:
            #discard spaces
            while input_str[ndx] == " ":
                ndx+=1
            #an operand is expected here go into error mode
            #if an operator is present.
            if is_operator(input_str[ndx]):
                state=ERROR
            rhs=''
            has_decimal = False
            #append digits to the rhs as they are found
            while ndx < len(input_str) and (is_number(input_str[ndx]) or input_str[ndx] == '.'):
                if input_str[ndx] == '.':
                    if not has_decimal:
                        rhs += input_str[ndx]
                        has_decimal = True
                    else:
                        print("Syntax error parsing decimal.")
                        quit()
                elif input_str[ndx] == '(':
                    rhs= parse(input_str, ndx+1)
                else:
                    rhs += input_str[ndx]
                ndx += 1

        
            expression[1][1] = rhs
            rhs=''
            #discard spaces again if we end up at another number
            # go to error state

            while ndx < len(input_str) and input_str[ndx] == ' ':
                ndx +=1
            if ndx < len(input_str) and (is_number(input_str[ndx]) or input_str[ndx] == '.'):
                state = ERROR

            expression_stack.append(expression)
            expression = [ '', ['', '']]
            state=COMPUTE
        elif state == ERROR:
            print("Syntax error.")
            quit()
         

    #now evaluate the stack

    final_result = 0
    tmp_result = ''
    pending_expr = []

    while len(expression_stack) > 0:
        expr = expression_stack.pop()
        if expr[1][1] == '':
            expr[1][1] = tmp_result

        if can_eval(expr):
            tmp_result = evaluate(expr[1][0], expr[1][1], expr[0])
        else:
            if expr[1][0] == '':
                pending_expr.append(expr)
               
    while len(pending_expr):
        expr=pending_expr.pop()
        expr[1][0] = tmp_result
        tmp_result = evaluate(expr[1][0], expr[1][1], expr[0])

    return tmp_result

input_str = input("> ")
print(parse(input_str, start=0))

